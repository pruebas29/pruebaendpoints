﻿using AutoMapper;
using SampleEndpointApp.Authors;
using SampleEndpointApp.DomainModel;

namespace SampleEndpointApp
{
    
    // clase que permite el mapeo
    public class AutoMapping : Profile
    {
        
        public AutoMapping()
        {
            CreateMap<CreateAuthorCommand, Author>();
            CreateMap<UpdateAuthorCommand, Author>();

            CreateMap<Author, CreateAuthorResult>();
            CreateMap<Author, UpdatedAuthorResult>();
            CreateMap<Author, AuthorListResult>();
            CreateMap<Author, AuthorResult>();
        }
    }
}
